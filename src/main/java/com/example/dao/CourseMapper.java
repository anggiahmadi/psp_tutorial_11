package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.CourseModel;
import com.example.model.StudentModel;

@Mapper
public interface CourseMapper {
	@Select("SELECT id_course, nama, sks FROM course WHERE id_course = #{idCourse}")
	@Results(value = { @Result(property = "idCourse", column = "id_course"),
			@Result(property = "nama", column = "nama"), @Result(property = "sks", column = "sks"),
			@Result(property = "students", column = "id_course", javaType = List.class, many = @Many(select = "selectStudents")) })
	CourseModel selectCourse(@Param("idCourse") String idCourse);

	@Select("SELECT id_course, nama, sks FROM course")
	@Results(value = { @Result(property = "npm", column = "npm"), @Result(property = "nama", column = "nama"),
			@Result(property = "sks", column = "sks"),
			@Result(property = "students", column = "id_course", javaType = List.class, many = @Many(select = "selectStudents")) })
	List<CourseModel> selectAllCourses();

	@Insert("INSERT INTO course (npm, nama, sks) VALUES (#{npm}, #{nama}, #{sks})")
	void addCourse(CourseModel course);

	@Delete("DELETE FROM course WHERE id_course = #{idCourse}")
	void deleteCourse(String idCourse);

	@Update("UPDATE course SET nama = #{nama}, sks = #{sks} WHERE id_course = #{idCourse}")
	void updateCourse(CourseModel course);

	@Select("SELECT student.npm, name, gpa " + "FROM studentcourse JOIN student "
			+ "on studentcourse.npm = student.npm " + "where studentcourse.id_course = #{idCourse}")
	List<StudentModel> selectStudents(@Param("idCourse") String idCourse);
}
